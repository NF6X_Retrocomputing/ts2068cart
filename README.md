# ts2068cart: An EPROM-based Command Cartridge for the Timex Sinclair 2068 Computer
## by Mark J. Blair <nf6x@nf6x.net>

This board allows many 28-pin EPROMs, EEPROMs and mask ROMs to be used
in the cartridge slot of the Timex Sinclair 2068 computer. The
installed memory may be mapped into any or all of the 8k banks of the
dock space. Write access is supported for EEPROM memories.

![](ts2068cart-top.png?raw=true)

[Video of the card on YouTube](https://www.youtube.com/watch?v=EYMBuVz32ZE)

## Supported Memory Chips

Chip  | Size (bytes) | Type     | Notes
------|--------------|----------|-----------------------------------------
2764  | 8k           | EPROM    |
27128 | 16k          | EPROM    |
27256 | 32k          | EPROM    |
27512 | 64k          | EPROM    |
23128 | 16k          | Mask ROM | i.e. Spectrum ROM chip
23256 | 32k          | Mask ROM |
2864  | 8k           | EEPROM   | Read/write (see datasheet for algorithm)
28256 | 32k          | EEPROM   | Read/write (see datasheet for algorithm)

One or two jumpers need to be installed at JP1 to configure the memory
type. A summary table is provided on the back side of the board:

![](ts2068cart-bot.png?raw=true)

Also install one or more jumpers at JP2 to map the memory chip into
the desired 8k dock space banks. 

## Schematic Diagram

![](ts2068cart-sch.png?raw=true)

## Suggested Parts (not including memory chip)

Refs    | Manufacturer          | Part Number    | Comments
--------|-----------------------|----------------|--------------------------------------
U1      | Texas Instruments     | SN74LS32N      | No socket: limited vertical clearance
U2      | Texas Instruments     | SN74LS156N     | No socket: limited vertical clearance
U3      | 3M                    | 4828-6000-CP   | Socket for memory chip
C1-C3   | Kemet                 | C320C104M5R5TA | 
R1-R2   | Stackpole Electronics | CF14JT10K0     | 
JP1-JP2 | Sullins               | PBC36DAAN      | Cut to length; 2.5 boards per strip
Jumpers | Sullins               | QPC02SXGN-RC   |

## About the Design

This board was designed in Kicad 2014-12-16 BZR 5324 on a Mac.
Design rules are 6 mil line, 6 mil trace, 13 mil via hole. It is
suitable for fabrication at OSH Park.

ts2068cart is a public domain open source hardware design by
Mark J. Blair <nf6x@nf6x.net>, created in August, 2015.

It is offered with no license restrictions, and no warranty.

Use it at your own risk.
